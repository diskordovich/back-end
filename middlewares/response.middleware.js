const responseMiddleware = (req, res, next) => {
   if(res.err){
       const {message} = res.err
       res.status(400).json({
           error:true,
           message
       })
   }
   res.status(200).json(res.data)
   next()
}

exports.responseMiddleware = responseMiddleware;