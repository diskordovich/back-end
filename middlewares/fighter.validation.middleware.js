const { fighter } = require('../models/fighter');
const {FightRepository} = require('../repositories/fightRepository')
const createFighterValid = (req, res, next) => {
    try
    {
        if(req.body.id){
            throw new Error('Error: "Id" field cannot be included')
        }
        if (!req.body.name||req.body.name=='') {
            throw new Error('Error: "Name" field is empty. ')

        };
        if (!req.body.power||req.body.name=='') {
            throw new Error('Error: "Power" field is empty. ')
        };
        if (!req.body.defense||req.body.name=='') {
            throw new Error('Error: "Defence" field is empty. ')
        };
    }
    catch(err){
        res.err = err
    }
    finally{
        next();
    }
    
}

const updateFighterValid = (req, res, next) => {
    try
    {
        if (!req.body.name||req.body.name=='') {
            throw new Error('Error: "Name" field is empty. ')
        };
        if (!req.body.power||req.body.name=='') {
            throw new Error('Error: "Power" field is empty. ')
        };
        if (!req.body.defense||req.body.name=='') {
            throw new Error('Error: "Defence" field is empty.')
        };
        next();
    }
    catch(err){
        res.err = err
    }
    finally{
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;