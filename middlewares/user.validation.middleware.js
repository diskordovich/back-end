const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try
    {
        if (!req.body.firstName||req.body.firstName=='') {
            throw new Error('Error: "First name" field is empty. ')
        };
        if (!req.body.lastName||req.body.lastName=='') 
            throw new Error('Error: "Last name" field is empty.')
            
        if (!req.body.password||req.body.password=='')
            throw new Error('Error: "Password" field is empty') 
    if (String(req.body.password).length<3) 
            throw new Error('Password has to be 3 or more characters')
        if (!(/^[\w.\-]{0,25}@(gmail)\.com$/i).test(req.body.email))
            throw new Error('Error: "Email" field is not valid') 
            
        if (!req.body.email||req.body.email==="")
            throw new Error('Error: "Email" field is empty') 
            
        if (!(/^\+380\d{9}$/i).test(req.body.phoneNumber))
            throw new Error('Error: "Phone number" field is not valid') 
            
        if (!req.body.phoneNumber||req.body.phoneNumber==="")
            throw new Error('Error: "Phone number" field is empty') 
            
        next();
    }
    catch(err){
        res.err = err
    }
    finally{
        next()
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try
    {
        if (!req.body.firstName||req.body.firstName=='') {
            throw new Error ('Error: "First name" field is empty. ')
        };
        if (!req.body.lastName||req.body.lastName=='') 
            throw new Error('Error: "Last name" field is empty. ') 
        if (!req.body.password||req.body.password=='') 
            throw new Error('Error: "Password" field is empty. ') 
            
        if (String(req.body.password).length<3) 
            throw new Error('Error: Password has to be 3 or more characters. ') 
        if (!(/^[\w.\-]{0,25}@(gmail)\.com$/i).test(req.body.email))
            throw new Error('Error: "Email" field is not valid. ') 
            
        if (!req.body.email||req.body.email==="") 
            throw new Error('Error: "Email" field is empty. ')
        if (!(/^\+380\d{9}$/i).test(req.body.phoneNumber)) 
            throw Error('Error: "Phone number" field is not valid') 
        if (!req.body.phoneNumber||req.body.phoneNumber==="")
            throw new Error('Error: "Phone number" field is empty')  
            
        
    }
    catch(err){
        res.err = err
    }
    finally{
        next()
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

/* Наличие полей
Формат полей:
    email - только gmail почты
    phoneNumber: +380xxxxxxxxx
    power - число и < 100
Id в body запросов должен отсутствовать
Лишние поля не должны пройти в БД 
let user = {firstName: firstName,
     lastName: lastName,
      email:email,
       phoneNumber:phoneNumber,
        password:password} ;
*/