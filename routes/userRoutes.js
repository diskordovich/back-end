const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid, (req, res, next) => {
  try {
      if(res.err) throw res.err
      let data = req.body
      let user = UserService.createUser(data)
      res.data = user
  }
  catch (err) {
      res.err = err
  }
  finally {
      next();
  }
}, responseMiddleware);

router.get('/',(req, res, next)=>{
  try {
    console.log("ALL")
    if(res.err) throw res.err
    let users = UserService.getAll()
    res.data = users
  } 
  catch (err) {
    res.err = err;
  }
  finally {
    next();
  }
}, responseMiddleware) 

router.get('/:id',(req, res, next)=>{
  try {
    if(res.err) throw res.err
    let id = req.params.id;
    let user = UserService.getUser(id);
    res.data = user;
  } 
  catch (err) {
    res.err = err;
  }
  finally {
    next();
  }
}, responseMiddleware) 

router.put('/:id',updateUserValid,(req, res, next)=>{
  try {
    if(res.err) throw res.err
    const id = req.params.id
    const data = req.body
    const user = UserService.updateUser(id, data)
    res.data = user;
  } 
  catch (err) {
    res.err = err;
  }
  finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id',(req, res, next)=>{
  try {
    if(res.err) throw res.err
    const id = req.params.id;
    const user = UserService.deleteUser(id);
    res.data = user;
  } 
  catch (err) {
    res.err = err;
  }
  finally {
    next();
  }
}, responseMiddleware)


module.exports = router;