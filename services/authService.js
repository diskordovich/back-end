const UserService = require('./userService');

class AuthService {
    login(userData) {
        let {email, password}= userData;
        email = email.toLowerCase();
        const user = UserService.search({email, password});
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();